package chat.logic;

import java.util.Collections;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

public class GameManager {
    private Set<String> gamesSet;
    private Hashtable gamesTable;
    private int version;

    public GameManager() {
        gamesSet = new HashSet<String>();
        gamesTable = new Hashtable<String, GameData>();
        version = 0;
    }

    public void addGame (String gamename, GameData game) {
        gamesSet.add(gamename);
        gamesTable.put(gamename, game);
        updateVersion();
    }

    public void removeGame (String gamename) {
        gamesSet.remove(gamename);
        updateVersion();
    }

    public Set<String> getGames() {
        return Collections.unmodifiableSet(gamesSet);
    }

    public GameData getGame(String gamename) {
        return (GameData)gamesTable.get(gamename);
    }

    public boolean isGameExists (String gamename) {
        return gamesSet.contains(gamename);
    }

    public int getVersion() {
        return version;
    }

    private void updateVersion() {
        ++version;
    }
}
