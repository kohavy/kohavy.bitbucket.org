/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.logic;

import java.util.HashSet;
import ticktactoe.GameLogic.Game;
import java.util.Set;
import ticktactoe.GameLogic.Token;

/**
 *
 * @author גיל כוכב
 */
public class GameData {
    private Game m_Game;
    private Set<String> usersSet;
    private String statusCode;
    private Boolean needUpdate=false;
    
    public GameData(Game i_Game)
    {
        m_Game = i_Game;
        usersSet = new HashSet<String>();
        statusCode = "";
    }
    
    public void MakeMove(String i_Code)
    {
        m_Game.MakeMove(i_Code);
    }
    
    public String getCode()
    {
        return statusCode;
    }
    
    public String getOpponent()
    {
        return m_Game.GetSecondPlayer();
    }
    
    public void addOpponent(String i_Name)
    {
        m_Game.AddPlayer(i_Name, Token.O);
    }
    
    public void updateStatusCode()
    {
        statusCode = m_Game.StatusCode();
    }
}
