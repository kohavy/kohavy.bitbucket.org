
package chat.servlets;

import chat.Constants;
import chat.logic.GameData;
import chat.logic.GameManager;
import chat.utils.SessionUtils;
import chat.utils.ServletUtils;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "MoveServlet", urlPatterns = {"/gamemove"})
public class MoveServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        GameManager gameManager = ServletUtils.getGameManager(getServletContext());
        String gamename=SessionUtils.getGamename(request);
        String moveCode = request.getParameter("selectedsquare");
        String gameCode = "";
        GameData game = gameManager.getGame(gamename);
        PrintWriter out = response.getWriter();
        
        game.MakeMove(moveCode);
        game.updateStatusCode();
        gameCode = game.getCode();
        
        out.println("<script>document.getElementById(\"statuscode\").innerHTML=\""+
                gameCode+"\";</script>");
        out.println("<script>document.getElementById(\"needupdate\").innerHTML=\"y\";</script>");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
