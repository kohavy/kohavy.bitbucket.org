/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.servlets;

import chat.Constants;
import chat.logic.GameData;
import chat.logic.GameManager;
import chat.utils.SessionUtils;
import chat.utils.ServletUtils;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import ticktactoe.GameLogic.Game;
import ticktactoe.GameLogic.Token;
import ticktactoe.Schema.SchemaManager;

@WebServlet(name = "GameServlet", urlPatterns = {"/game"})
public class GameServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        GameManager gameManager = ServletUtils.getGameManager(getServletContext());

        String username = SessionUtils.getUsername(request);
        if (username == null) {
            response.sendRedirect("index.html");
        }
        else
        {
            String actionCode = request.getParameter("actioncode");
            String gameName = "";
            Boolean bSuccessful = true;
            
            if(actionCode == "j")
            {
                gameName = request.getParameter("gamename");
            }
            else
            {
                Game openGame = null;
                    
                if(actionCode == "x")
                {
                    JFileChooser Opener = new JFileChooser();
                    FileNameExtensionFilter filter = new FileNameExtensionFilter("XML files", "xml");
                    Opener.setFileFilter(filter);
                    int ReturnVal = Opener.showOpenDialog(null);

                    if(ReturnVal == JFileChooser.APPROVE_OPTION)
                    {
                        try
                        {
                            openGame = new SchemaManager().LoadGameFromFile(Opener.getSelectedFile().getPath(), 
                                    Opener.getSelectedFile().getName().replaceAll(".xml", ""));

                            if(openGame == null)
                            {
                                bSuccessful=false;
                            }
                            else
                            {
                                openGame.ClearPlayers();
                            }
                        }
                        catch(Exception e)
                        {
                            bSuccessful=false;
                        }
                    }
                    else
                    {
                        bSuccessful=false;
                    }
                }
                else if(actionCode == "n")
                {
                    openGame = new Game();
                }
                
                if(bSuccessful)
                {
                    gameName = username;
                    openGame.AddPlayer(username, Token.X);
                    gameManager.addGame(username, new GameData(openGame));
                }
            }
            
            if(bSuccessful)
            {
                request.getSession(true).setAttribute(Constants.GAMENAME, gameName);
                System.out.println("Adding game from " + username);
                System.out.println("Server Game version: " + gameManager.getVersion()
                    + ", User '" + username + "'");
                response.sendRedirect("gameboard.html");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}