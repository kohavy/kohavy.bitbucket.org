package ticktactoe.GameLogic;

import ticktactoe.Schema.PlayerType;

/**
 * PlayerHuman is a a derived class from player.
 * this class handles the human player's actions and properties.
 */
public class PlayerHuman extends Player 
{
    
    private boolean valid = false;
    
    public PlayerHuman(String i_Name, Token i_Token)
    {   
        m_Name = i_Name;
        m_AssignedToken = i_Token;
    }
    
    public PlayerHuman(ticktactoe.Schema.Player i_Player)
    {
        super(i_Player);
    }

    @Override
    public ticktactoe.Schema.Player ToSchema()
    {
        ticktactoe.Schema.Player SchemaPlayer = new ticktactoe.Schema.Player();
        
        SchemaPlayer.setType(PlayerType.HUMAN);
        SchemaPlayer.setValue(m_AssignedToken.GetValue());
        SchemaPlayer.setName(m_Name);
        
        return SchemaPlayer;
    }
    
    @Override
    public Cell Play(MasterBoard i_Master, Board i_Board) 
    {
//        m_Cell = consoleUI.requestPlay("Make a play : row col");
        
        
        
        valid = isVaildMove(m_Cell, i_Board);
        
        while(!valid)
        {
            System.out.println("Already taken. try again");
//            m_Cell = consoleUI.requestPlay("Make a play : row col");
            valid = isVaildMove(m_Cell, i_Board);
        }
        
        i_Board.UpdateBoard(m_Cell, m_AssignedToken);
            
        return m_Cell;
    }
    
    @Override
    public Board GetBoard(MasterBoard i_Master)
    {
        return i_Master.SelectBoard(m_Cell);
    }
    
}
