package ticktactoe.GameLogic;


/**
 * MasterBoard class handles the responsibilities for managing all board games. 
 */
public class MasterBoard {
    
    private Board[][] m_MasterBoardMatrix;
    
    public MasterBoard()
    {
        m_MasterBoardMatrix = new Board[Board.DIMENSION][];
        
        for(int nRow=0; nRow<Board.DIMENSION;nRow++)
        {
            m_MasterBoardMatrix[nRow] = new Board[Board.DIMENSION];
            
            for(int nCol=0; nCol<Board.DIMENSION;nCol++)
            {
                m_MasterBoardMatrix[nRow][nCol]=new Board();
            }
        }
    }
    
    public String StatusCode()
    {
        String strReturned = "";
        
        for(int row=0; row<Board.DIMENSION; row++)
        {
            for(int col=0; col<Board.DIMENSION; col++)
            {
                strReturned += m_MasterBoardMatrix[row][col].StatusCode();
            }
        }
        
        return strReturned;
    }
    
    public Board SelectBoard(int i_Row, int i_Col)
    {
        return m_MasterBoardMatrix[i_Row][i_Col];
    }
    
    public Board SelectBoard(Cell i_Cell)
    {
        return SelectBoard(i_Cell.GetRow(), i_Cell.GetCol());
    }
    
    public Cell SelectCell(Board i_Board)
    {
        for(int row=0; row<Board.DIMENSION; row++)
        {
            for(int col=0; col<Board.DIMENSION; col++)
            {
                if(m_MasterBoardMatrix[row][col].equals(i_Board))
                {
                    return new Cell(row,col);
                }
            }
        }
        
        return new Cell(0,0);
    }

    void setBoard(ticktactoe.Schema.Board i_Board, Board i_Base) {
        Board SelectedBoard = m_MasterBoardMatrix[i_Board.getRow() - 1][i_Board.getCol() - 1];
        SelectedBoard.setBoard(i_Board);
        
        if(SelectedBoard.isBoardWon())
        {
            i_Base.UpdateBoard(new Cell(i_Board.getRow() - 1, i_Board.getCol() - 1), 
                    Token.GetToken(i_Board.getWinner().value()));
        }
    }
    
    public ticktactoe.Schema.Tictactoe.Boards ToSchema()
    {
        ticktactoe.Schema.Tictactoe.Boards SchemaBoard = 
                new ticktactoe.Schema.Tictactoe.Boards();
        
        for(int row=0; row<Board.DIMENSION; row++)
        {
            for(int col=0; col<Board.DIMENSION; col++)
            {
                if(!m_MasterBoardMatrix[row][col].isBoardEmpty())
                {
                    SchemaBoard.getBoard().add(
                            m_MasterBoardMatrix[row][col].ToSchema(row, col));
                }
            }
        }
        
        return SchemaBoard;
    }
    
}
