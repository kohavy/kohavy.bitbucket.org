package ticktactoe.GameLogic;

import java.util.ArrayList;
import java.util.Random;
import ticktactoe.Schema.PlayerType;

/**
 * PlayerComputer is a a derived class from player.
 * this class handles the computer player's actions and properties.
 */
public class PlayerComputer extends Player {
    
    public PlayerComputer()
    {
        randomSelectComputerSuperVillain();
        getComputerToken();
    }
    
    public PlayerComputer(Token i_Token)
    {
        randomSelectComputerSuperVillain();
        m_AssignedToken = i_Token;
    }
    
    public PlayerComputer(ticktactoe.Schema.Player i_Player)
    {
        super(i_Player);
        randomSelectComputerSuperVillain();
    }

    @Override
    public Cell Play(MasterBoard i_Master, Board i_Board)
    {
        Board B = i_Board;
        
        if(B == null)
        {
            getComputerPlay(i_Master);
            B=i_Master.SelectBoard(m_Cell);
        }
        
        getComputerPlay(B);
        B.UpdateBoard(m_Cell, m_AssignedToken);
        return m_Cell;
    }
    
    @Override
    public Board GetBoard(MasterBoard i_Master)
    {
        getComputerPlay(i_Master);
        return i_Master.SelectBoard(m_Cell);
    }

    @Override
    public ticktactoe.Schema.Player ToSchema()
    {
        ticktactoe.Schema.Player SchemaPlayer = new ticktactoe.Schema.Player();
        
        SchemaPlayer.setType(PlayerType.COMPUTER);
        SchemaPlayer.setValue(m_AssignedToken.GetValue());
        
        return SchemaPlayer;
    }

    public Cell SelectFreeBoardCell(MasterBoard i_MasterBoard)
    {
        getComputerPlay(i_MasterBoard);
        
        return m_Cell;
    }

    private void randomSelectComputerSuperVillain()
    {
        String SuperVillains[] = {"HAL","SKYNET","WOPR","V.I.K.I","ZIGGY"};
        Random randomGenerator = new Random(System.currentTimeMillis());
        int randomChoice = randomGenerator.nextInt(SuperVillains.length- 1) ;
        m_Name = SuperVillains[randomChoice];
        System.out.println(m_Name + " is getting ready to destroy you");
    }

    private void getComputerToken()
    {
        m_AssignedToken = Token.GetOtherToken();
    }

    private void getComputerPlay(Board i_Board)
    {
        ArrayList<Cell> emptyCellsList;
        int randomChoice;
        
        emptyCellsList = new ArrayList<>();
        produceEmptyCellsList(i_Board, emptyCellsList);
        
        Random randomGenerator = new Random(System.currentTimeMillis());
        randomChoice = randomGenerator.nextInt(emptyCellsList.size());
        m_Cell = emptyCellsList.get(randomChoice);
        
    }

    private void getComputerPlay(MasterBoard i_MasterBoard)
    {
        ArrayList<Cell> FreeBoardsList;
        int randomChoice;
        
        FreeBoardsList = new ArrayList<>();
        produceFreeBoardsList(i_MasterBoard, FreeBoardsList);
        
        Random randomGenerator = new Random(System.currentTimeMillis());
        randomChoice = randomGenerator.nextInt(FreeBoardsList.size());
        m_Cell = FreeBoardsList.get(randomChoice);
        
    }

    private void produceEmptyCellsList(Board i_Board, 
            ArrayList<Cell> emptyCellsList)
    {
        Cell cell;

        for (int row = 0; row < Board.DIMENSION; row++)
        {
            for (int col = 0; col < Board.DIMENSION; col++)
            {
                if (i_Board.getBoardMatrix()[row][col] == Token.FREE)
                {
                    cell = new Cell(row, col);
                    emptyCellsList.add(cell);
                }

            }
        }

    }

    private void produceFreeBoardsList(MasterBoard i_MasterBoard, 
            ArrayList<Cell> FreeBoardsList) {
        Board board;

        for (int row = 0; row < Board.DIMENSION; row++)
        {
            for (int col = 0; col < Board.DIMENSION; col++)
            {
                board = i_MasterBoard.SelectBoard(row, col);
                
                if (!board.isBoardFull())
                {
                    FreeBoardsList.add(new Cell(row, col));
                }

            }
        }
    }
    
}
