package ticktactoe.GameLogic;

/**
 *  The Cell class represents a single unit out of 9 (3 by 3) in a board.
 */
public class Cell 
{
    private int m_Row;
    private int m_Col;
    
    public Cell(int i_Row,int i_Col)
    {
        SetCell(i_Row, i_Col);
    }
    
    public Cell(ticktactoe.Schema.Cell i_Cell)
    {
        SetCell(i_Cell.getRow() - 1, i_Cell.getCol() - 1);
    }
    
    public Cell(ticktactoe.Schema.Tictactoe.Game.CurrentBoard i_Board)
    {
        SetCell(i_Board.getRow() - 1, i_Board.getCol() - 1);
    }
    
    public ticktactoe.Schema.Cell ToSchema()
    {
        ticktactoe.Schema.Cell SchemaCell = new ticktactoe.Schema.Cell();
        
        SchemaCell.setRow(m_Row + 1);
        SchemaCell.setCol(m_Col + 1);
        
        return SchemaCell;
    }
    
    public final void SetCell(int i_Row,int i_Col)
    {
        m_Row = i_Row;
        m_Col = i_Col;
    }
    
     public int GetRow()
    {
        return m_Row;
    }
     
     public String CellPosition()
     {
         String strRow = "";
         String strCol = "";
         
         switch(m_Row)
         {
             case 0:
             {
                 strRow = "top";
                 break;
             }
             case 1:
             {
                 strRow = "middle";
                 break;
             }
             case 2:
             {
                 strRow = "bottom";
                 break;
             }
             default:
             {
                 break;
             }
         }
         
         switch(m_Col)
         {
             case 0:
             {
                 strCol = "left";
                 break;
             }
             case 1:
             {
                 strCol = "middle";
                 break;
             }
             case 2:
             {
                 strCol = "right";
                 break;
             }
             default:
             {
                 break;
             }
         }
         
         return strRow + "-" + strCol;
     }
     
     public int GetCol()
    {
        return m_Col;
    }
}
