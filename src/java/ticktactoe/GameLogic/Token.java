package ticktactoe.GameLogic;

import ticktactoe.Schema.GameValue;

/**
 * Token class holds the game's known markings to be filled in a cell.
 */
public enum Token
{

    X("X"), O("O"), FREE(" "), DRAW("=");
    private String m_Token;
    public static boolean m_TokenWasAssigned = false;
    private static Token m_OtherToken;

    private Token(String i_String)
    {
        m_Token = i_String;
    }
    
    public GameValue GetValue()
    {
        return GameValue.fromValue(m_Token);
    }
    
    public Token OtherToken()
    {
        if(m_Token.equals("X"))
            return Token.O;
        else if(m_Token.equals("O"))
            return Token.X;
        else
            return this;
    }

    @Override
    public String toString()
    {
        return m_Token;
    }
    
    public String GetTokenString()
    {
        return m_Token;
    }
    
    public static Token GetToken(String i_String)
    {
        m_TokenWasAssigned = true;
        
        if("X".equals(i_String))
        {
            m_OtherToken = O;
            return X;
        }
        else
        {
            m_OtherToken = X;
            return O;
        }    
    }
    
    public static Token GetOtherToken()
    {
        return m_OtherToken;
    }
}
