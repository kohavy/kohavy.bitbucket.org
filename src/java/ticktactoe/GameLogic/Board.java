package ticktactoe.GameLogic;


/**
 * Board class handles the responsibilities of a single board game.
 * this class represents a single unit out of 9 (3 by 3) in the masterboard. 
 */
public class Board {
 
    public static final char DIMENSION = 3;
    private Token[][] m_BoardMatrix;
    private int m_CellsPlayedCount;
    private Token m_WinningToken;
    
    
    // ctor
    public Board()
    {
        
        initializeBoard();  
        m_CellsPlayedCount = 0;
        m_WinningToken = Token.FREE;
    }
    
    public String StatusCode()
    {
        String strReturned = "";
        
        if((m_WinningToken == Token.X) ||
                        (m_WinningToken == Token.O))
        {
            strReturned += m_WinningToken.GetTokenString();
        }
        else
        {
            strReturned += "E";
        }
        
        for(int row=0; row<DIMENSION; row++)
        {
            for(int col=0; col<DIMENSION; col++)
            {
                if((m_BoardMatrix[row][col] == Token.X) ||
                        (m_BoardMatrix[row][col] == Token.O))
                {
                    strReturned += m_BoardMatrix[row][col].GetTokenString();
                }
            }
        }
        
        return strReturned;
    }
    
    public void setBoard(ticktactoe.Schema.Board i_Board)
    {
        for(ticktactoe.Schema.Cell currentCell : i_Board.getCell())
        {
            m_BoardMatrix[currentCell.getRow() - 1][currentCell.getCol() - 1] = 
                    Token.GetToken(currentCell.getValue().value());
            m_CellsPlayedCount++;
        }
    }
    
    public ticktactoe.Schema.Board ToSchema(int i_Row, int i_Col)
    {
        ticktactoe.Schema.Board SchemaBoard = new ticktactoe.Schema.Board();
        SchemaBoard.setRow(i_Row + 1);
        SchemaBoard.setCol(i_Col + 1);
        
        if(isBoardWon())
        {
            SchemaBoard.setWinner(m_WinningToken.GetValue());
        }
        
        for(int row=0; row<DIMENSION; row++)
        {
            for(int col=0; col<DIMENSION; col++)
            {
                if((m_BoardMatrix[row][col] == Token.X) ||
                        (m_BoardMatrix[row][col] == Token.O))
                {
                    ticktactoe.Schema.Cell newCell = 
                            new Cell(row,col).ToSchema();
                    newCell.setValue(m_BoardMatrix[row][col].GetValue());
                    SchemaBoard.getCell().add(newCell);
                }
            }
        }
        
        return SchemaBoard;
    }
    
    public boolean isBoardWon() {

        if(m_WinningToken == Token.FREE)
        {
            if (checkDiagnols() || checkRows() || checkCols()) {
                return true;
            } else {
                return false;
            }
        }
        else if(m_WinningToken == Token.DRAW)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    public Token BoardWonBy()
    {
        return m_WinningToken;
    }

    private boolean checkDiagnols() {
        if(checkDiagnolTopToBottom() || checkDiagnolBottomToTop())
            return true;
        else
            return false;
    }
    private boolean checkDiagnolTopToBottom(){
        if (m_BoardMatrix[0][0] == m_BoardMatrix[1][1] && 
                m_BoardMatrix[1][1] == m_BoardMatrix[2][2])
        {
            if(m_BoardMatrix[0][0] == Token.O || m_BoardMatrix[0][0] == Token.X)
            {
                m_WinningToken = m_BoardMatrix[0][0];
                return true;
            }
            else
            {
                return false;
            }
        }
        else
            return false;
    }
    
    private boolean checkDiagnolBottomToTop(){
        if (m_BoardMatrix[2][0] == m_BoardMatrix[1][1] && 
                m_BoardMatrix[1][1] == m_BoardMatrix[0][2])
        {
            if(m_BoardMatrix[2][0] == Token.O || m_BoardMatrix[2][0] == Token.X)
            {
                m_WinningToken = m_BoardMatrix[2][0];
                return true;
            }
            else
            {
                return false;
            }
        }
        else
            return false;
    }

    private boolean checkRows() {
        boolean bFoundRow = false;
        
        for(int nCurrRow=0;nCurrRow<DIMENSION;nCurrRow++)
        {
            bFoundRow = bFoundRow || checkRow(nCurrRow);
        }
        
        if(bFoundRow)
            return true;
        else
            return false;
    }
    
    private boolean checkRow(int i_Row)
    {
        if (m_BoardMatrix[i_Row][0] == m_BoardMatrix[i_Row][1] && 
                m_BoardMatrix[i_Row][1] == m_BoardMatrix[i_Row][2])
        {
            if(m_BoardMatrix[i_Row][0] == Token.O || 
                    m_BoardMatrix[i_Row][0] == Token.X)
            {
                m_WinningToken = m_BoardMatrix[i_Row][0];
                return true;
            }
            else
            {
                return false;
            }
        }
        else
            return false;
    }

    private boolean checkCols() {
        boolean bFoundCol = false;
        
        for(int nCurrCol=0;nCurrCol<DIMENSION;nCurrCol++)
        {
            bFoundCol = bFoundCol || checkCol(nCurrCol);
        }
        
        if(bFoundCol)
            return true;
        else
            return false;
    }
    
    private boolean checkCol(int i_Col)
    {
        if (m_BoardMatrix[0][i_Col] == m_BoardMatrix[1][i_Col] && 
                m_BoardMatrix[1][i_Col] == m_BoardMatrix[2][i_Col])
        {
            if(m_BoardMatrix[0][i_Col] == Token.O || 
                    m_BoardMatrix[0][i_Col] == Token.X)
            {
                m_WinningToken = m_BoardMatrix[0][i_Col];
                return true;
            }
            else
            {
                return false;
            }
        }
        else
            return false;
    }


    private void initializeBoard() {
        m_BoardMatrix = new Token[DIMENSION][DIMENSION];
        for (int row = 0; row < m_BoardMatrix.length; row++) {
            for (int col = 0; col < m_BoardMatrix.length; col++) {
                m_BoardMatrix[row][col] = Token.FREE;
            }
        }
    }

    public Token getCell(int i_Row, int i_Col) {
        return (m_BoardMatrix[i_Row][i_Col]);
    }

    public Token getCell(Cell i_Cell) {
        return getCell(i_Cell.GetRow(), i_Cell.GetCol());
    }

    public void UpdateBoard(Cell i_Cell, Token i_Token) {
        m_BoardMatrix[i_Cell.GetRow()][i_Cell.GetCol()] = i_Token;
        m_CellsPlayedCount++;
    }

    public Token[][] getBoardMatrix() {
        return m_BoardMatrix;
    }

    void MarkVictory(Token m_AssignedToken)
    {
        resetBoard();
        markPlayerVictory(m_AssignedToken);
    }

    private void resetBoard()
    {
        initializeBoard();
    }

    private void markPlayerVictory(Token m_AssignedToken)
    {
        switch(m_AssignedToken)
        {
            case X: 
            {
                m_BoardMatrix[0][0] = m_AssignedToken; 
                m_BoardMatrix[1][1] = m_AssignedToken;
                m_BoardMatrix[2][2] = m_AssignedToken; 
                m_BoardMatrix[0][2] = m_AssignedToken;
                m_BoardMatrix[2][0] = m_AssignedToken;
            }
                break;
                case O: 
            {
                m_BoardMatrix[0][0] = m_AssignedToken; 
                m_BoardMatrix[0][1] = m_AssignedToken;
                m_BoardMatrix[0][2] = m_AssignedToken; 
                m_BoardMatrix[1][0] = m_AssignedToken;
                m_BoardMatrix[2][0] = m_AssignedToken; 
                m_BoardMatrix[2][1] = m_AssignedToken;
                m_BoardMatrix[2][2] = m_AssignedToken; 
                m_BoardMatrix[1][2] = m_AssignedToken;
            }
        }
    }

    public boolean isBoardFull()
    {
        if(m_CellsPlayedCount == DIMENSION*DIMENSION)   
            return true;
        else
            return false;       
    }

    public boolean isBoardEmpty()
    {
        if(m_CellsPlayedCount == 0)   
            return true;
        else
            return false;       
    }
    
}
