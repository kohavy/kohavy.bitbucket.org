package ticktactoe.GameLogic;


/**
 * Player is a base class for two different derived classes (human/computer). 
 * this class handles the responsibilities for overlapping behavior.
 */
public abstract class Player 
{

    protected String m_Name;
    protected Cell m_Cell;
    protected Token m_AssignedToken;
    protected int m_Id;
    
    static int m_PlayerInstances = 0;
    
    public Player()
    {
        updatePlayerCount();
        updatePlayerId();
    }
    
    public Player(ticktactoe.Schema.Player i_Player)
    {
        m_Name = i_Player.getName();
        m_AssignedToken = Token.GetToken(i_Player.getValue().value());
    }
    
    abstract Cell Play(MasterBoard i_Master, Board i_Board);
    
    abstract Board GetBoard(MasterBoard i_Master);
            
    abstract ticktactoe.Schema.Player ToSchema();

    private static void updatePlayerCount()
    {
        m_PlayerInstances++;
    }

    private void updatePlayerId()
    {
        m_Id = m_PlayerInstances;
    }
    
    protected boolean isVaildMove(Cell i_Cell, Board i_Board) 
    {
        if(isFree(i_Board.getCell(i_Cell.GetRow(), i_Cell.GetCol())))
            return true;
        else
            return false;
                    
    }
    
    public Token GetToken()
    {
        return m_AssignedToken;
    }
    
    public String GetName()
    {
        return m_Name;
    }
    
    protected boolean isFree(Token i_Token)
    {
        if(i_Token == Token.FREE) 
            return true;
        else
            return false;
    }

    
}
