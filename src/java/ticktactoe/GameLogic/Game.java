package ticktactoe.GameLogic;

import java.util.ArrayList;

/**
 *  the Game class represents the game's structure and flow.
 */
public class Game {
    
    public enum GameType { HUMANvsHUMAN, HUMANvsPC; }
    private static Game Instance;
    private MasterBoard m_Board;
    private Board m_CurrentBoard;
    private Board m_BaseBoard;
    private Cell m_SelectedCell;
    private boolean isGamePlaying = true;
    private boolean isGameNew = true;
    private String m_GameName;
    private int m_Turn;
     
    private ArrayList<Player> m_Players;
    private GameType m_GameType;
    
    
    public static Game GetNew1PlayerGame(String i_PlayerName, Token i_PlayerToken)
    {
        Instance = new Game(i_PlayerName, i_PlayerToken);
        return Instance;
    }
    
    public static Game GetNew2PlayerGame(String i_Player1Name, String i_Player2Name, Token i_Player1Token)
    {
        Instance = new Game(i_Player1Name, i_Player2Name, i_Player1Token);
        return Instance;
    }
    
    public String StatusCode()
    {
        String strReturned = m_Board.StatusCode();
        
        if(isGamePlaying)
        {
            Player p = GetCurrentPlayer();
            
            if(m_CurrentBoard == null)
            {
                strReturned += "EE";
            }
            else
            {
                strReturned += m_SelectedCell.GetRow();
                strReturned += m_SelectedCell.GetCol();
            }
            
            strReturned += p.GetToken().GetTokenString();
        }
        else if(m_BaseBoard.isBoardWon())
        {
            Token T = m_BaseBoard.BoardWonBy();
            
            strReturned += T.GetTokenString();
            strReturned += T.GetTokenString();
            
            if(m_Players.get(m_Turn%2).m_AssignedToken == T)
            {
                strReturned += m_Players.get(m_Turn%2).m_Name;
            }
            else
            {
                strReturned += m_Players.get((m_Turn+1)%2).m_Name;
            }
        }
        else
        {
            strReturned += "DD";
        }
        
        return strReturned;
    }
    
    public static Game GetInstance()
    {
        return Instance;
    }
    
    public boolean IsGameOn()
    {
        return isGamePlaying;
    }
   
    public Game(ticktactoe.Schema.Tictactoe i_tictactoe, String i_GameName) throws Exception
    {
        int PlayerCounter = i_tictactoe.getPlayers().getPlayer().size();
        m_Players = new ArrayList<>();
        m_Board = new MasterBoard();
        m_BaseBoard = new Board();
        m_Turn = 0;
        isGameNew = false;
        m_GameName = i_GameName;
        
        m_SelectedCell = new Cell(i_tictactoe.getGame().getCurrentBoard());
        m_CurrentBoard = m_Board.SelectBoard(m_SelectedCell);
        
        for(ticktactoe.Schema.Board currentBoard : 
                i_tictactoe.getBoards().getBoard())
        {
            m_Board.setBoard(currentBoard, m_BaseBoard);
        }
        
        if(i_tictactoe.getPlayers().getPlayer().size() != 2)
        {
            throw new Exception();
        }
        
        for(ticktactoe.Schema.Player currentPlayer : 
                i_tictactoe.getPlayers().getPlayer())
        {
            if(currentPlayer.getType() == ticktactoe.Schema.PlayerType.HUMAN)
            {
                m_Players.add(new PlayerHuman(currentPlayer));
            }
            else
            {
                m_Players.add(new PlayerComputer(currentPlayer));
            }
            
            if(currentPlayer.getValue() == 
                    i_tictactoe.getGame().getCurrentTurn())
            {
                m_Turn = PlayerCounter;
            }
            
            PlayerCounter++;
        }
    }
    
    public ticktactoe.Schema.Tictactoe toSchema()
    {
        ticktactoe.Schema.Tictactoe Schema = 
                new ticktactoe.Schema.Tictactoe();
        ticktactoe.Schema.Tictactoe.Players SchemaPlayers = 
                new ticktactoe.Schema.Tictactoe.Players();
        ticktactoe.Schema.Tictactoe.Game SchemaGame = 
                new ticktactoe.Schema.Tictactoe.Game();
        ticktactoe.Schema.Tictactoe.Game.CurrentBoard SchemaBoard = 
                new ticktactoe.Schema.Tictactoe.Game.CurrentBoard();
        Cell BoardCell = m_Board.SelectCell(m_CurrentBoard);
        
        for(Player currentPlayer : m_Players)
        {
            SchemaPlayers.getPlayer().add(currentPlayer.ToSchema());
        }
        
        SchemaBoard.setRow(BoardCell.GetRow() + 1);
        SchemaBoard.setCol(BoardCell.GetCol() + 1);
        SchemaGame.setCurrentBoard(SchemaBoard);
        SchemaGame.setCurrentTurn(
                m_Players.get(m_Turn % 2).ToSchema().getValue());
        
        Schema.setPlayers(SchemaPlayers);
        Schema.setBoards(m_Board.ToSchema());
        Schema.setGame(SchemaGame);
        
        return Schema;
    }
    
    public Game()
    {
        //currentGameUI = GameSettingsSingleton.getInstance().getGameUI();
        m_Players = new ArrayList<>();
        m_Board = new MasterBoard();
        m_BaseBoard = new Board();
        m_Turn = 0;     
    }
    
    public String GetSecondPlayer()
    {
        return m_Players.get(1).GetName();
    }
    
    public void AddPlayer(String i_PlayerName, Token i_PlayerToken)
    {
        m_Players.add(new PlayerHuman(i_PlayerName, i_PlayerToken));
    }
    
    public void ClearPlayers()
    {
        m_Players.clear();
        isGamePlaying = false;
    }
    
    private Game(String i_PlayerName, Token i_PlayerToken)
    {
        this();
        
        m_Players.add(new PlayerHuman(i_PlayerName, i_PlayerToken));
        m_Players.add(new PlayerComputer(i_PlayerToken.OtherToken()));
    }
    
    private Game(String i_Player1Name, String i_Player2Name, Token i_Player1Token)
    {
        this();
        
        m_Players.add(new PlayerHuman(i_Player1Name, i_Player1Token));
        m_Players.add(new PlayerHuman(i_Player2Name, i_Player1Token.OtherToken()));
    }
    
    public void MakeMove(String i_Code)
    {
        Board playedBoard = m_Board.SelectBoard(
                Integer.parseInt(i_Code.substring(0, 1)), 
                Integer.parseInt(i_Code.substring(1, 2)));
        Cell playedCell = new Cell(
                Integer.parseInt(i_Code.substring(2, 3)), 
                Integer.parseInt(i_Code.substring(3, 4)));
        MakeMove(playedBoard, playedCell);
    }
    
    public void MakeMove(Board i_Board, Cell i_Cell)
    {
        Player P = GetCurrentPlayer();
        Cell BaseCell = m_Board.SelectCell(i_Board);
        i_Board.UpdateBoard(i_Cell, P.GetToken());
        
        if(m_BaseBoard.getCell(BaseCell) == Token.FREE)
        {
            if(i_Board.isBoardWon())
            {
                m_BaseBoard.UpdateBoard(BaseCell, i_Board.BoardWonBy());
            }
            else if(i_Board.isBoardFull())
            {
                m_BaseBoard.UpdateBoard(BaseCell, Token.DRAW);
            }
        }
        
        m_SelectedCell = i_Cell;
        m_CurrentBoard = m_Board.SelectBoard(i_Cell);
        
        if(m_CurrentBoard.isBoardFull())
        {
            m_CurrentBoard = null;
        }
        
        m_Turn++;
        
        if((m_BaseBoard.isBoardWon())||(m_BaseBoard.isBoardFull()))
        {
            isGamePlaying = false;
        }
    }
    
    public void MakeComputerMove()
    {
        Player P = GetCurrentPlayer();
        Cell BaseCell = m_Board.SelectCell(m_CurrentBoard);
        Cell NextCell;
        
        if(P instanceof PlayerComputer)
        {
            if(m_CurrentBoard == null)
            {
                m_CurrentBoard = P.GetBoard(m_Board);
            }
            
            NextCell = P.Play(m_Board, m_CurrentBoard);
        
            if(m_BaseBoard.getCell(BaseCell) == Token.FREE)
            {
                if(m_CurrentBoard.isBoardWon())
                {
                    m_BaseBoard.UpdateBoard(BaseCell, m_CurrentBoard.BoardWonBy());
                }
                else if(m_CurrentBoard.isBoardFull())
                {
                    m_BaseBoard.UpdateBoard(BaseCell, Token.DRAW);
                }
            }

            m_CurrentBoard = m_Board.SelectBoard(NextCell);

            if(m_CurrentBoard.isBoardFull())
            {
                m_CurrentBoard = null;
            }

            m_Turn++;

            if((m_BaseBoard.isBoardWon())||(m_BaseBoard.isBoardFull()))
            {
                isGamePlaying = false;
            }
        }
    }
    
    public Player GetCurrentPlayer()
    {
        return m_Players.get(m_Turn % 2);
    }
    
    public Board GetCurrentBoard()
    {
        return m_CurrentBoard;
    }
    
    public Board SelectBoard(int i_Row, int i_Col)
    {
        return m_Board.SelectBoard(i_Row, i_Col);
    }
    
    public Board SelectBoard(Cell i_Cell)
    {
        return m_Board.SelectBoard(i_Cell);
    }
    
    public String GetFinishMessage()
    {
        if(isGamePlaying)
        {
            return "";
        }
        else if(m_BaseBoard.isBoardWon())
        {
            Token T = m_BaseBoard.BoardWonBy();
            
            if(m_Players.get(m_Turn%2).m_AssignedToken == T)
            {
                return m_Players.get(m_Turn%2).m_Name + " wins!";
            }
            else
            {
                return m_Players.get((m_Turn+1)%2).m_Name + " wins!";
            }
        }
        else
        {
            return "It's a draw!";
        }
    }
    
}
