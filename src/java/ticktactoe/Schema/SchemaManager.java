package ticktactoe.Schema;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import javax.xml.bind.Marshaller;
import ticktactoe.GameLogic.Game;

/**
 * SchemaManager handles XML related procedures: marshalling/unmarshalling/validation.
 */
public class SchemaManager
{
    String xmlFilePath = null;
    Boolean validFormat = false;
    
    public Game LoadGameFromFile(String i_FilePath, String i_GameName) throws Exception {
        
        Tictactoe gameSchema = extractObjectFromFile(i_FilePath);
        Game ReturnedGame;
        
        if (!isValidFormat(gameSchema))
        {
            return null;
        }
        
        try
        {
            ReturnedGame = createGameFromSchema(gameSchema, i_GameName);
        }
        catch(Exception e)
        {
            throw new Exception("Invalid XML format");
        }

        return ReturnedGame;
    }
    
     public void SaveGameToFile(Game i_Game, String i_FilePath)
     {
          Tictactoe tictactoe = i_Game.toSchema();
          
	  try {
 
		File file = new File(i_FilePath);
		JAXBContext jaxbContext = JAXBContext.newInstance(Tictactoe.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
 
		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
 
		jaxbMarshaller.marshal(tictactoe, file);
 
	      } catch (JAXBException e) {
                   System.out.println("ERROR: " + e.getMessage());
                   validFormat = false;
	      }
     }

    private Tictactoe extractObjectFromFile(String i_FilePath) {
        
        Tictactoe gameSchema = null;
       
        if (i_FilePath != null)
        {
            gameSchema = UnmarshallXML(i_FilePath);
        }
      
        return gameSchema;
        
    }

    private Tictactoe UnmarshallXML(String i_FilePath) {
        
        File file;
        Tictactoe gameSchema = null;
        
        try
        {
            file = new File(i_FilePath);
            JAXBContext jaxbContext = JAXBContext.newInstance(Tictactoe.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            gameSchema = (Tictactoe) unmarshaller.unmarshal(file);
            validFormat = true;
            return gameSchema;

        } catch (JAXBException ex)
        {
            String message = ex.getMessage();
            if(message == null)
            {
                System.out.println("ERROR: Game file not found");
            }
            else
            {
                System.out.println("ERROR: " + message);
            }
           validFormat = false;
        } 
        return gameSchema;
    }

    private boolean isValidFormat(Tictactoe gameSchema)
    {
        return validFormat;
    }

    private Game createGameFromSchema(Tictactoe gameSchema, String i_GameName) throws Exception
    {
        return new Game(gameSchema, i_GameName);
    }
    
    
}
